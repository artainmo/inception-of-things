# inception-of-things

This repository is used by the bonus part of the [inception-of-things 42 school project](https://github.com/artainmo/inception-of-things).

It contains an app that will be run inside a kubernetes container defined [here](https://github.com/artainmo/inception-of-things/tree/main/bonus).<br>
The running app will be synchronized with this repository, by using a gitlab CI/CD pipeline that calls [Argo CD](https://github.com/artainmo/DevOps/tree/main/kubernetes#argo-cd) for synchronization each time a change is made in this repository.<br>


Instead of combining GitLab CI/CD with Argo CD for automatic synchronization we could simply setup Argo CD with automatic synchronization as we did [here](https://github.com/artainmo/inception-of-things/tree/main/p3). However in this exercise we had to use GitLab.<br>
Instead of using Argo CD with GitLab CI/CD we could apparently use GitLab agents to perform synchronization instead. However in this exercise we had to use Argo CD.
